package Task1;

import java.util.Scanner;

public class reverse_array 
{
	public static void main(String[] args) 
	{
	
		int counter, index=0, temp,last_index;
		int number[] = new int[100];
		Scanner scanner = new Scanner(System.in);
		System.out.print("How many elements you want to enter: ");
		counter = scanner.nextInt();
		for(index=0; index<counter; index++)
		{
		    System.out.print("Enter Array Element_"+(index+1)+" : ");
		    number[index] = scanner.nextInt();
		}

		last_index = index - 1;     
		index = 0;        
		while(index<last_index)
		{
	  	   temp = number[index];
		   number[index] = number[last_index];
		   number[last_index] = temp;
		   index++;
		   last_index--;
		}

		System.out.print("Reversed array: ");
		for(index=0; index<counter; index++)
		{
		   System.out.print(number[index]+ "  ");
		}       
	 
	}
}
