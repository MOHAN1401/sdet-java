public class TimePeriod {

    int seconds;
    int minutes;
    int hours;

    public timePeriod(int hours, int minutes, int seconds)
    {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }
    public static void main(String[] args)
    {
        timePeriod start = new timePeriod(12, 34, 55);
        timePeriod stop = new timePeriod(8, 12, 15); 
        timePeriod diff;
        diff = difference(start, stop);
        
        System.out.printf("TIME DIFFERENCE: %d:%d:%d - ", start.hours, start.minutes, start.seconds);
        System.out.printf("%d:%d:%d ", stop.hours, stop.minutes, stop.seconds);
        System.out.printf("= %d:%d:%d\n", diff.hours, diff.minutes, diff.seconds);
    }
    public static timePeriod difference(timePeriod start, timePeriod stop)
    {
        timePeriod diff = new timePeriod(0, 0, 0);
        if(stop.seconds > start.seconds)
        {
            --start.minutes;
            start.seconds += 60;
        }
        diff.seconds = start.seconds - stop.seconds;
        if(stop.minutes > start.minutes)
        {
            --start.hours;
            start.minutes += 60;
        }
        diff.minutes = start.minutes - stop.minutes;
        diff.hours = start.hours - stop.hours;

        return(diff);
    }
}


