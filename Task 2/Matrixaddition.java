public class Matrixaddition 
{
	public static void main(String[] args)
	{
        int rows = 2, columns = 4;
    
        int[][] matrix1 = { {1, 2, 3, 4}, {2, 3, 5, 2} };
        int[][] matrix2 = { {2, 3, 4, 5}, {2, 2, 4, -4} };
  
        int[][] sum = new int[rows][columns];
        for(int row = 0; row < rows; row++) 
        {
            for (int column = 0; column < columns; column++) 
            {
                sum[row][column] = MatrixA[row][column] + MatrixB[row][column];
            }
        }
        System.out.println("Sum of the matrices is: ");
        for(int row = 0; row < rows; row++) 
        {
            for (int column = 0; column < columns; column++) 
            {
                System.out.print(sum[row][column] + "    ");
            }
            System.out.println();
        }
    }
}

