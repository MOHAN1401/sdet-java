
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
public class StringToDate
{
   public static void main(String args[])
   {
	   String testDateString = "02-04-2014 23:37:50";
	   DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
       try
       {
    	   Date d = df.parse(testDateString);
           System.out.println("Date: " +d);
           System.out.println("Date in dd-MM-yyyy HH:mm:ss format is: "+df.format(d));
       }
       catch (Exception ex )
       {
          System.out.println(ex);
       }
   }
}