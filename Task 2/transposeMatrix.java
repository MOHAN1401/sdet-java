public class transposeMatrix 
{
	    public static void main(String[] args)
	    {
	        int row = 3, column = 3;
	        int[][] matrix = { {1, 4, 7}, {2, 5, 8}, {3, 6, 9} };
	        display(matrix);

	        int[][] transpose = new int[column][row];
	        for(int i = 0; i < row; i++) 
	        {
	            for (int j = 0; j < column; j++) 
	            {
	                transpose[j][i] = matrix[i][j];
	            }
	        }
	        display(transpose);
	    }

	    public static void display(int[][] matrix) 
	    {
	        System.out.println("The matrix is: ");
	        for(int[] row : matrix) 
	        {
	            for (int column : row) 
	            {
	                System.out.print(column + "    ");
	            }
	            System.out.println();
	        }
	    }
}
