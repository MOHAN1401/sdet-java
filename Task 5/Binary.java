package Task5;

import java.util.Scanner;

public class Binary 
{
	public static void main(String[] args) 
	{
		Scanner in =new Scanner(System.in);
		System.out.println("Enter the number");
		int number=in.nextInt();
		boolean isBinary = true;		 
        int copyOfNumber = number;
        while (copyOfNumber != 0)
        {
            int temp = copyOfNumber%10;  
            if(temp > 1)
            {
                isBinary = false;
                break;
            }
            else
            {
                copyOfNumber = copyOfNumber/10;    
            }
        }
        if (isBinary)
        {
            System.out.println(number+" is a binary number");
        }
        else
        {
            System.out.println(number+" is not a binary number");
        }
    }
}
