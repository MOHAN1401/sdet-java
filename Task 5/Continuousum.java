package Task5;

import java.util.Scanner;

public class Continuousum 
{
	public static void main(String[] args) 
	{
		Scanner in=new Scanner(System.in);
		System.out.println("Enter the no. of elements");
		int size=in.nextInt();
		int inputArray[]=new int[size];
		System.out.println("Enter the elements");
		for(int index=0;index<size;index++)
			inputArray[index]=in.nextInt();
		System.out.println("Enter the number");
		int inputNumber=in.nextInt();
		int sum = inputArray[0];
        int start = 0; 
        for (int i = 1; i < inputArray.length; i++)
        { 
            sum = sum + inputArray[i];
            while(sum > inputNumber && start <= i-1)
            { 
                sum = sum - inputArray[start];
                start++;
            }
             if(sum == inputNumber)
            {
                System.out.println("Continuous sub inputArray of whose sum of "+inputNumber+" is ");
 
                for (int j = start; j <= i; j++)
                {
                    System.out.print(inputArray[j]+" ");
                }
 
                System.out.println();
            }
        }		
	}
}
