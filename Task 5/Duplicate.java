package Task5;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class Duplicate 
{	
	public static void main(String[] args) 
	{
		HashMap<Character, Integer> charCountMap = new HashMap<Character, Integer>();	 
		Scanner in =new Scanner(System.in);
		String inputString="butter better";
        char[] strArray = inputString.toCharArray();
        for (char c : strArray)
        {
            if(charCountMap.containsKey(c))
            { 
                charCountMap.put(c, charCountMap.get(c)+1);
            }
            else
            {
                charCountMap.put(c, 1);
            }
        }
        
        Set<Character> charsInString = charCountMap.keySet(); 
        System.out.println("Duplicate Characters In "+inputString);  
        for (Character ch : charsInString)
        {
            if(charCountMap.get(ch) > 1)
            {  
                System.out.println(ch +" : "+ charCountMap.get(ch));
            }
        }
    }
 }