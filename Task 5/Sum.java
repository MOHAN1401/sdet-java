package Task5;

import java.util.Arrays;
import java.util.Scanner;

public class Sum 
{
	public static void main(String[] args) 
	{
		Scanner in=new Scanner(System.in);
		System.out.println("Enter the no. of elements");
		int size=in.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter the elements");
		for(int index=0;index<size;index++)
			arr[index]=in.nextInt();
		System.out.println("Enter the number");
		int inputNumber=in.nextInt();
        Arrays.sort(arr);
        System.out.println("Pairs of elements whose sum is "+inputNumber+" are : "); 
        int i = 0;
        int j = arr.length-1;
        while (i < j)
        { 
            if(arr[i]+arr[j] == inputNumber)
            { 
                System.out.println(arr[i]+" + "+arr[j]+" = "+inputNumber);
                i++; 
                j--;
            }
            else if (arr[i]+arr[j] < inputNumber)
            {
                i++;
            }
            else if (arr[i]+arr[j] > inputNumber)
            { 
                j--;
            }
        }
	}
}
