package Task5;

import java.util.HashMap;

public class occurence 
{
	public static void main(String[] args) 
	{
         String inputString="java j2EE java jsp J2EE";
        HashMap<Character, Integer> charCountMap = new HashMap<Character, Integer>();
        char[] strArray = inputString.toCharArray();  
        for (char c : strArray)
        {
            if(charCountMap.containsKey(c))
            {
                charCountMap.put(c, charCountMap.get(c)+1);
            }
            else
            {
                charCountMap.put(c, 1);
            }
        }  
        System.out.println(inputString+" : "+charCountMap);
    }
}
