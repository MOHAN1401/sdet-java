package Task5;

import java.util.Arrays;

public class zero 
{
	public static void main(String[] args) 
	{
        int inputArray[]={14, 0, 5, 2, 0, 3, 0};
        int counter = 0; 
        for (int i = 0; i < inputArray.length; i++)
        { 
            if(inputArray[i] != 0)
            { 
                inputArray[counter] = inputArray[i]; 
                counter++;
            }
        }
        
         while (counter < inputArray.length)
        {
            inputArray[counter] = 0;
            counter++;
        }
        System.out.println("Bringing zeros to back:  "+Arrays.toString(inputArray));
        
        int count = inputArray.length-1; 
        for (int i = inputArray.length-1; i >= 0; i--)
        {
            
          if(inputArray[i] != 0)
            { 
                inputArray[count] = inputArray[i];
 
                count--;
            }
        }
 
        while (count >= 0)
        {
            inputArray[count] = 0;
 
            count--;
        }
 
        System.out.println("Bringing zeros to front: "+Arrays.toString(inputArray));
	}
}
